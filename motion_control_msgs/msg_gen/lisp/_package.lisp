(cl:defpackage motion_control_msgs-msg
  (:use )
  (:export
   "<JOINTPOSITIONS>"
   "JOINTPOSITIONS"
   "<JOINTACCELERATIONS>"
   "JOINTACCELERATIONS"
   "<JOINTEFFORTS>"
   "JOINTEFFORTS"
   "<JOINTVELOCITIES>"
   "JOINTVELOCITIES"
  ))

